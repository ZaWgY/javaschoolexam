package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement == null || statement.equals("")) return null;
        String[] parts = splitExpression(statement);
        if(Arrays.asList(parts).contains(",")) return null;
        Stack<Double> s = new Stack<>();
        try {
            for (int i = 0; i < parts.length; i += 2) {
                if (i == 0 || parts[i - 1].equals("+")) {
                    s.push(Double.valueOf(parts[i]));
                } else  if (parts[i].equals("(")) {
                    int index = statement.indexOf(')', i);
                    if (index == -1) return null;
                    s.push(Double.parseDouble(evaluate(statement.substring(i+2, index))));
                    switch (parts[i - 1]) {
                        case "+": {
                            double tmpOne = s.pop();
                            double tmpTwo = s.pop();
                            s.push(tmpOne + tmpTwo);
                            break;
                        }
                        case "/": {
                            double tmpOne = s.pop();
                            double tmpTwo = s.pop();
                            if (tmpOne == 0) return null;
                            s.push(tmpTwo / tmpOne);
                            break;
                        }
                        case "-": {
                            double tmpOne = s.pop();
                            double tmpTwo = s.pop();
                            s.push(tmpTwo - tmpOne);
                            break;
                        }
                        case "*": {
                            double tmpOne = s.pop();
                            double tmpTwo = s.pop();
                            s.push(tmpTwo * tmpOne);
                            break;
                        }
                    }
                    i = index-1;
                }
                else if (parts[i - 1].equals("-")) {
                    s.push(-Double.valueOf(parts[i]));
                } else if (parts[i - 1].equals("*")) {
                    s.push(s.pop() * Double.parseDouble(parts[i]));
                } else if (parts[i - 1].equals("/")) {
                    double tmpD = s.pop();
                    s.push(tmpD / Double.parseDouble(parts[i]));
                }
            }
        } catch (java.lang.NumberFormatException e) {
            return null;
        }

        double r = 0;
        while(!s.isEmpty()) {
            double tmpD = s.pop();
            r += tmpD;
        }
        String result = r + "";
        if(r % 1 == 0){
            return result.substring(0,result.length()-2);
        }
        return result;
    }

    public String[] splitExpression(String statement) {
        ArrayList<String> a = new ArrayList<>();
        StringBuilder tmp = new StringBuilder();
        char[] statmentChars = statement.toCharArray();
        for (int i=0; i< statmentChars.length;i++) {
            char currentChar = statmentChars[i];
            if(Character.isDigit(currentChar) || currentChar == '.') tmp.append(currentChar);
            else {
                a.add(tmp.toString());
                if(i != statement.length()-1) {
                    tmp.delete(0, tmp.length());
                }
                a.add(currentChar+"");
            }
        }
        a.add(tmp.toString());
        while (a.remove(""));
        return a.toArray(new String[0]);
    }

}
