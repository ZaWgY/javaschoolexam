package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || (Math.sqrt(8*inputNumbers.size()+1) % 1 != 0)){
            throw new CannotBuildPyramidException();
        }
        int cntr = 0;
        int maxN = (int) (Math.sqrt(8*inputNumbers.size()+1)-1)/2;
        int base = maxN*2-1;
        int[][] result = new int[maxN][base];
        ArrayList<Integer> sortedNumbers = new ArrayList<>(inputNumbers);
        sortedNumbers.sort(Comparator.naturalOrder());
        for (int i = 0; i < maxN; i++){
            int zeroCounter = (base - (i+1) - i)/2;
            for (int j =0; j < zeroCounter; j++){
                result[i][j] = 0;
            }
            for (int j = zeroCounter; j < base-zeroCounter; j++){
                if (((base-1)/2)%2 == 0) {
                    if((j % 2 == 0 && i % 2 == 0) || (j % 2 == 1 && i % 2 != 0) ) {
                        result[i][j] = sortedNumbers.get(cntr++);
                    } else {
                        result[i][j] = 0;
                    }

                }
                else {
                    if( (i % 2 == 1 && j % 2 == 0) || (i % 2 == 0 && j % 2 == 1) ) {
                        result[i][j] = sortedNumbers.get(cntr++);
                    } else {
                        result[i][j] = 0;
                    }
                }
            }
            for (int j = base-zeroCounter; j < base; j++){
                result[i][j] = 0;
            }
        }
        return result;
    }


}
